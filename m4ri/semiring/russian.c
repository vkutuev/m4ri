/*******************************************************************
*
*                 M4RI: Linear Algebra over GF(2)
*
*    Copyright (C) 2007, 2008 Gregory Bard <bard@fordham.edu>
*    Copyright (C) 2008-2010 Martin Albrecht <M.R.Albrecht@rhul.ac.uk>
*    Copyright (C) 2018 Peter Schäfer <pete.schaefer@gmail.com>
*    Copyright (C) 2018 Egor Basharin <eaniconer@gmail.com>
*
*  Distributed under the terms of the GNU General Public License (GPL)
*  version 2 or higher.
*
*    This code is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*  The full text of the GPL is available at:
*
*                  http://www.gnu.org/licenses/
*
********************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "russian.h"
#include "or.h"

#include <m4ri/graycode.h>

//  Create table row from two previous rows:
//  T[i] = T[i1] + T[i2]
void mzd_sr_sum_table_row(word* ti, const word* ti1, const word* ti2,
                            wi_t const wide, word const mask_begin, word const mask_end)
{
    *ti++ = (*ti2++ | *ti1++) & mask_begin;

    wi_t j;
    for(j = 1; j + 8 <= wide - 1; j += 8) {
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
      *ti++ = *ti2++ | *ti1++;
    }
    switch(wide - j) {
    case 8:  *ti++ = *ti2++ | *ti1++;
    case 7:  *ti++ = *ti2++ | *ti1++;
    case 6:  *ti++ = *ti2++ | *ti1++;
    case 5:  *ti++ = *ti2++ | *ti1++;
    case 4:  *ti++ = *ti2++ | *ti1++;
    case 3:  *ti++ = *ti2++ | *ti1++;
    case 2:  *ti++ = *ti2++ | *ti1++;
    case 1:  *ti = (*ti2 | *ti1) & mask_end;
    }
}

//  Copy Table row from Matrix
//  T[i] = m
void mzd_sr_copy_table_row(word* ti, const word* m,
                            wi_t const wide, word const mask_begin, word const mask_end)
{
    memcpy(ti, m, sizeof(word)*wide);
    ti[0] &= mask_begin;
    ti[wide-1] &= mask_end;
}

//  return the least significant bit
#define lsb(i) ((i) & -(i))

/**
 * this bit twiddling trick is due to Bill Gosper
 * @return the next number with the same number of bits
 */
int snoob(int i)
{
    int least = lsb(i);
    int ripple = i + least;
    return (((ripple ^ i) >> 2) / least) | ripple;
}


void mzd_sr_make_table(mzd_t const *M, rci_t r, rci_t c, int k, mzd_t *T)
{
    wi_t const homeblock = c / m4ri_radix;
    word const mask_end = __M4RI_LEFT_BITMASK(M->ncols % m4ri_radix);
    word const pure_mask_begin = __M4RI_RIGHT_BITMASK(m4ri_radix - (c % m4ri_radix));
    word const mask_begin = (M->width - homeblock != 1) ? pure_mask_begin : pure_mask_begin & mask_end;
    wi_t const wide = M->width - homeblock;

    /*
     * The elegant construction using Gray codes doesn't work in our case
     *  (because we have no subtraction, only addition).
     *
     *  Instead, we construct the entries in order of increasing Hamming weights (number of set bits).
     *  1 bit entries are copied from M, 2 bit entries are sums of two previous rows, etc...
     */
    int const twokay = __M4RI_TWOPOW(k);
    rci_t i;

    //  1 bit
    for(int j=0; j < k; ++j)
    {
        i = __M4RI_TWOPOW(j);
        rci_t const rowneeded = r + j;
        assert(rowneeded < M->nrows);

        word *ti = T->rows[i] + homeblock;
        word *m = M->rows[rowneeded] + homeblock;

        mzd_sr_copy_table_row(ti,m, wide,mask_begin,mask_end);
    }
    //  2 bits, ...
    for(int h=2; h <= k; ++h) {
        //  iterate all integers with h bits, and < 2^k
        i = __M4RI_TWOPOW(h) - 1;
        for( ; i < twokay; i = snoob(i)) {
            rci_t least = lsb(i);
            rci_t rest = i-least;

            //  T[least] and T[rest] have already been calculated
            word *ti = T->rows[i] + homeblock;
            word *ti1 = T->rows[least] + homeblock;
            word *ti2 = T->rows[rest] + homeblock;

            mzd_sr_sum_table_row(ti,ti1,ti2, wide,mask_begin,mask_end);
        }
    }

    __M4RI_DD_MZD(T);
}

mzd_t *mzd_sr_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k) {
  rci_t a = A->nrows;
  rci_t c = B->ncols;

  if(A->ncols != B->nrows)
    m4ri_die("mzd_mul_m4rm: A ncols (%d) need to match B nrows (%d).\n", A->ncols, B->nrows);
  if (C == NULL) {
    C = mzd_init(a, c);
  } else {
    if (C->nrows != a || C->ncols != c)
      m4ri_die("mzd_mul_m4rm: C (%d x %d) has wrong dimensions.\n", C->nrows, C->ncols);
  }
  return _mzd_sr_mul_m4rm(C, A, B, k, TRUE);
}

mzd_t *mzd_sr_addmul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k) {
  rci_t a = A->nrows;
  rci_t c = B->ncols;

  if(C->ncols == 0 || C->nrows == 0)
    return C;

  if(A->ncols != B->nrows)
    m4ri_die("mzd_mul_m4rm A ncols (%d) need to match B nrows (%d) .\n", A->ncols, B->nrows);
  if (C == NULL) {
    C = mzd_init(a, c);
  } else {
    if (C->nrows != a || C->ncols != c)
      m4ri_die("mzd_mul_m4rm: C has wrong dimensions.\n");
  }
  return _mzd_sr_mul_m4rm(C, A, B, k, FALSE);
}

#define __M4RI_M4RM_NTABLES 8

mzd_t *_mzd_sr_mul_m4rm(mzd_t *C, mzd_t const *A, mzd_t const *B, int k, int clear) {
  /**
   * The algorithm proceeds as follows:
   *
   * Step 1. Make a Gray code table of all the \f$2^k\f$ linear combinations
   * of the \f$k\f$ rows of \f$B_i\f$.  Call the \f$x\f$-th row
   * \f$T_x\f$.
   *
   * Step 2. Read the entries
   *    \f$a_{j,(i-1)k+1}, a_{j,(i-1)k+2} , ... , a_{j,(i-1)k+k}.\f$
   *
   * Let \f$x\f$ be the \f$k\f$ bit binary number formed by the
   * concatenation of \f$a_{j,(i-1)k+1}, ... , a_{j,ik}\f$.
   *
   * Step 3. for \f$h = 1,2, ... , c\f$ do
   *   calculate \f$C_{jh} = C_{jh} + T_{xh}\f$.
   */

  rci_t        x[__M4RI_M4RM_NTABLES];
  //rci_t       *L[__M4RI_M4RM_NTABLES];
  word  const *t[__M4RI_M4RM_NTABLES];
  mzd_t       *T[__M4RI_M4RM_NTABLES];
#ifdef __M4RI_HAVE_SSE2
  mzd_t  *Talign[__M4RI_M4RM_NTABLES];
  int c_align = (__M4RI_ALIGNMENT(C->rows[0], 16) == 8);
#endif

  word *c;

  rci_t const a_nr = A->nrows;
  rci_t const b_nr = B->nrows;
  rci_t const a_nc = A->ncols;
  rci_t const b_nc = B->ncols;

  //    Short-Cut for small matrices
  if (b_nc < m4ri_radix-10 || a_nr < 16) {
    if(clear)
      return mzd_sr_mul_naive(C, A, B);
    else
      return mzd_sr_addmul_naive(C, A, B);
  }
  /* clear first */
  if (clear) {
    mzd_set_ui(C, 0);
  }


  const int blocksize = __M4RI_MUL_BLOCKSIZE;

  if(k==0) {
    /* __M4RI_CPU_L2_CACHE == 2^k * B->width * 8 * 8 */
    k = (int)log2((__M4RI_CPU_L2_CACHE/64)/(double)B->width);
    if ((__M4RI_CPU_L2_CACHE - 64*__M4RI_TWOPOW(k)*B->width) > (64*__M4RI_TWOPOW(k+1)*B->width - __M4RI_CPU_L2_CACHE))
      k++;

    rci_t const klog = round(0.75 * log2_floor(MIN(MIN(a_nr,a_nc),b_nc)));

    if(klog < k)
      k = klog;
  }
  if (k<2)
    k=2;
  else if(k>8)
    k=8;

    //    Downsizing for large matrices
    //
    //  If the M4RM tables don't fit into L1 cache..
    //  split into sub-windows and multiply column-band-wise
    rci_t row_max = (4*__M4RI_CPU_L1_CACHE) / __M4RI_TWOPOW(k);
    row_max = MAX(256,MIN(row_max,8192)) & ~(m4ri_radix-1);
    if (b_nc > row_max)
    {
        for(rci_t c0=0; c0 < b_nc; c0 += row_max)
        {
            rci_t c1 = MIN(b_nc,c0+row_max);
            mzd_t const *B1 = mzd_init_window_const(B,   0,   c0,   b_nr,  c1);
            mzd_t *C1 = mzd_init_window(            C,   0,   c0,   a_nr,  c1);

            _mzd_sr_mul_m4rm(C1, A, B1, k, FALSE);

            mzd_free_window((mzd_t*)B1);
            mzd_free_window(C1);
        }
        return C;
    }

  const wi_t wide = C->width;
  const word bm = __M4RI_TWOPOW(k)-1;

  rci_t *buffer = (rci_t*)m4ri_mm_malloc(__M4RI_M4RM_NTABLES * __M4RI_TWOPOW(k) * sizeof(rci_t));
  for(int z=0; z<__M4RI_M4RM_NTABLES; z++) {
//    L[z] = buffer + z*__M4RI_TWOPOW(k);
#ifdef __M4RI_HAVE_SSE2
    /* we make sure that T are aligned as C */
    Talign[z] = mzd_init(__M4RI_TWOPOW(k), b_nc+m4ri_radix);
    T[z] = mzd_init_window(Talign[z], 0, c_align*m4ri_radix, Talign[z]->nrows, b_nc + c_align*m4ri_radix);
#else
    T[z] = mzd_init(__M4RI_TWOPOW(k), b_nc);
#endif
  }

  /* process stuff that fits into multiple of k first, but blockwise (babystep-giantstep)*/
  int const kk = __M4RI_M4RM_NTABLES * k;
  assert(kk <= m4ri_radix);
  rci_t const end = a_nc / kk;

  for (rci_t giantstep = 0; giantstep < a_nr; giantstep += blocksize) {
    for(rci_t i = 0; i < end; ++i) {
#if __M4RI_HAVE_OPENMP
#pragma omp parallel for schedule(static,1)
#endif
      for(int z=0; z<__M4RI_M4RM_NTABLES; z++) {
        mzd_sr_make_table( B, kk*i + k*z, 0, k, T[z]);
      }

      const rci_t blockend = MIN(giantstep+blocksize, a_nr);
#if __M4RI_HAVE_OPENMP
#pragma omp parallel for schedule(static,512) private(x,t)
#endif
      for(rci_t j = giantstep; j < blockend; j++) {
        const word a = mzd_read_bits(A, j, kk*i, kk);

        switch(__M4RI_M4RM_NTABLES) {
        case 8: t[7] = T[ 7]->rows[ (a >> 7*k) & bm ];
        case 7: t[6] = T[ 6]->rows[ (a >> 6*k) & bm ];
        case 6: t[5] = T[ 5]->rows[ (a >> 5*k) & bm ];
        case 5: t[4] = T[ 4]->rows[ (a >> 4*k) & bm ];
        case 4: t[3] = T[ 3]->rows[ (a >> 3*k) & bm ];
        case 3: t[2] = T[ 2]->rows[ (a >> 2*k) & bm ];
        case 2: t[1] = T[ 1]->rows[ (a >> 1*k) & bm ];
        case 1: t[0] = T[ 0]->rows[ (a >> 0*k) & bm ];
          break;
        default:
          m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
        }

        c = C->rows[j];

        switch(__M4RI_M4RM_NTABLES) {
        case 8: _mzd_or_combine_8(c, t, wide); break;
        case 7: _mzd_or_combine_7(c, t, wide); break;
        case 6: _mzd_or_combine_6(c, t, wide); break;
        case 5: _mzd_or_combine_5(c, t, wide); break;
        case 4: _mzd_or_combine_4(c, t, wide); break;
        case 3: _mzd_or_combine_3(c, t, wide); break;
        case 2: _mzd_or_combine_2(c, t, wide); break;
        case 1: _mzd_or_combine(c, t[0], wide);
          break;
        default:
          m4ri_die("__M4RI_M4RM_NTABLES must be <= 8 but got %d", __M4RI_M4RM_NTABLES);
        }
      }
    }
  }

  /* handle stuff that doesn't fit into multiple of kk */
  if (a_nc%kk) {
    rci_t i;
    for (i = kk / k * end; i < a_nc / k; ++i) {
      mzd_sr_make_table( B, k*i, 0, k, T[0]);
      for(rci_t j = 0; j < a_nr; ++j) {
        x[0] = mzd_read_bits_int(A, j, k*i, k);
        c = C->rows[j];
        t[0] = T[0]->rows[x[0]];
        for(wi_t ii = 0; ii < wide; ++ii) {
          c[ii] |= t[0][ii];
        }
      }
    }
    /* handle stuff that doesn't fit into multiple of k */
    if (a_nc%k) {
      mzd_sr_make_table( B, k*(a_nc/k), 0, a_nc%k, T[0]);
      for(rci_t j = 0; j < a_nr; ++j) {
        x[0] = mzd_read_bits_int(A, j, k*i, a_nc%k);
        c = C->rows[j];
        t[0] = T[0]->rows[x[0]];
        for(wi_t ii = 0; ii < wide; ++ii) {
          c[ii] |= t[0][ii];
        }
      }
    }
  }

  for(int j=0; j<__M4RI_M4RM_NTABLES; j++) {
    mzd_free(T[j]);
#ifdef __M4RI_HAVE_SSE2
    mzd_free(Talign[j]);
#endif
  }
  m4ri_mm_free(buffer);

  __M4RI_DD_MZD(C);
  return C;
}

