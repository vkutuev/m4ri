/******************************************************************************
*
*            M4RI: Linear Algebra over GF(2)
*
*    Copyright (C) 2007 Gregory Bard <gregory.bard@ieee.org> 
*    Copyright (C) 2009-2013 Martin Albrecht <martinralbrecht+m4ri@googlemail.com>
*    Copyright (C) 2011 Carlo Wood <carlo@alinoe.com>
*    Copyright (C) 2018 Peter Schäfer <pete.schaefer@gmail.com>
*    Copyright (C) 2018 Egor Basharin <eaniconer@gmail.com>
*
*  Distributed under the terms of the GNU General Public License (GPL)
*  version 2 or higher.
*
*    This code is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*  The full text of the GPL is available at:
*
*                  http://www.gnu.org/licenses/
******************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __M4RI_HAVE_LIBPNG
#include <png.h>
#endif

#include "mzd_sr.h"

void mzd_sr_row_or(mzd_t *M, rci_t sourcerow, rci_t destrow) {
  mzd_sr_row_or_offset(M, destrow, sourcerow, 0);
}

mzd_t *mzd_or(mzd_t *ret, mzd_t const *left, mzd_t const *right) {
  if (left->nrows != right->nrows || left->ncols != right->ncols) {
    m4ri_die("mzd_add: rows and columns must match.\n");
  }
  if (ret == NULL) {
    ret = mzd_init(left->nrows, left->ncols);
  } else if (ret != left) {
    if (ret->nrows != left->nrows || ret->ncols != left->ncols) {
      m4ri_die("mzd_add: rows and columns of returned matrix must match.\n");
    }
  }
  return _mzd_or(ret, left, right);
}

mzd_t *_mzd_or(mzd_t *C, mzd_t const *A, mzd_t const *B) {
  rci_t const nrows = MIN(MIN(A->nrows, B->nrows), C->nrows);

  if (C == B) { //swap
    mzd_t const *tmp = A;
    A = B;
    B = tmp;
  }

  word const mask_end = C->high_bitmask;

  switch(A->width) {
  case 0:
    return C;
  case 1:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = ((A->rows[i][0] | B->rows[i][0]) & mask_end);
    }
    break;
  case 2:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = ((A->rows[i][1] | B->rows[i][1]) & mask_end);
    }
    break;
  case 3:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = ((A->rows[i][2] | B->rows[i][2]) & mask_end);
    }
    break;
  case 4:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] | B->rows[i][2];
      C->rows[i][3] = ((A->rows[i][3] | B->rows[i][3]) & mask_end);
    }
    break;
  case 5:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] | B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] | B->rows[i][3];
      C->rows[i][4] = ((A->rows[i][4] | B->rows[i][4]) & mask_end);
    }
    break;
  case 6:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] | B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] | B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] | B->rows[i][4];
      C->rows[i][5] = ((A->rows[i][5] | B->rows[i][5]) & mask_end);
    }
    break;
  case 7:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] | B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] | B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] | B->rows[i][4];
      C->rows[i][5] = A->rows[i][5] | B->rows[i][5];
      C->rows[i][6] = ((A->rows[i][6] | B->rows[i][6]) & mask_end);
    }
    break;
  case 8:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] | B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] | B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] | B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] | B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] | B->rows[i][4];
      C->rows[i][5] = A->rows[i][5] | B->rows[i][5];
      C->rows[i][6] = A->rows[i][6] | B->rows[i][6];
      C->rows[i][7] = ((A->rows[i][7] | B->rows[i][7]) & mask_end);
    }
    break;

  default:
    for(rci_t i = 0; i < nrows; ++i) {
      mzd_sr_or_even(C,i,0, A,i,0, B,i,0);
    }
  }

  __M4RI_DD_MZD(C);
  return C;
}

mzd_t *mzd_and(mzd_t *ret, mzd_t const *left, mzd_t const *right) {
  if (left->nrows != right->nrows || left->ncols != right->ncols) {
    m4ri_die("mzd_add: rows and columns must match.\n");
  }
  if (ret == NULL) {
    ret = mzd_init(left->nrows, left->ncols);
  } else if (ret != left) {
    if (ret->nrows != left->nrows || ret->ncols != left->ncols) {
      m4ri_die("mzd_add: rows and columns of returned matrix must match.\n");
    }
  }
  return _mzd_and(ret, left, right);
}

mzd_t *_mzd_and(mzd_t *C, mzd_t const *A, mzd_t const *B) {
  rci_t const nrows = MIN(MIN(A->nrows, B->nrows), C->nrows);

  if (C == B) { //swap
    mzd_t const *tmp = A;
    A = B;
    B = tmp;
  }

  word const mask_end = C->high_bitmask;

  switch(A->width) {
  case 0:
    return C;
  case 1:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = ((A->rows[i][0] & B->rows[i][0]) & mask_end);
    }
    break;
  case 2:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = ((A->rows[i][1] & B->rows[i][1]) & mask_end);
    }
    break;
  case 3:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = ((A->rows[i][2] & B->rows[i][2]) & mask_end);
    }
    break;
  case 4:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] & B->rows[i][2];
      C->rows[i][3] = ((A->rows[i][3] & B->rows[i][3]) & mask_end);
    }
    break;
  case 5:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] & B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] & B->rows[i][3];
      C->rows[i][4] = ((A->rows[i][4] & B->rows[i][4]) & mask_end);
    }
    break;
  case 6:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] & B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] & B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] & B->rows[i][4];
      C->rows[i][5] = ((A->rows[i][5] & B->rows[i][5]) & mask_end);
    }
    break;
  case 7:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] & B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] & B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] & B->rows[i][4];
      C->rows[i][5] = A->rows[i][5] & B->rows[i][5];
      C->rows[i][6] = ((A->rows[i][6] & B->rows[i][6]) & mask_end);
    }
    break;
  case 8:
    for(rci_t i = 0; i < nrows; ++i) {
      C->rows[i][0] = A->rows[i][0] & B->rows[i][0];
      C->rows[i][1] = A->rows[i][1] & B->rows[i][1];
      C->rows[i][2] = A->rows[i][2] & B->rows[i][2];
      C->rows[i][3] = A->rows[i][3] & B->rows[i][3];
      C->rows[i][4] = A->rows[i][4] & B->rows[i][4];
      C->rows[i][5] = A->rows[i][5] & B->rows[i][5];
      C->rows[i][6] = A->rows[i][6] & B->rows[i][6];
      C->rows[i][7] = ((A->rows[i][7] & B->rows[i][7]) & mask_end);
    }
    break;

  default:
    for(rci_t i = 0; i < nrows; ++i) {
      mzd_sr_and_even(C,i,0, A,i,0, B,i,0);
    }
  }

  __M4RI_DD_MZD(C);
  return C;
}

mzd_t *mzd_sr_mul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B) {
  if (C == NULL) {
    C = mzd_init(A->nrows, B->ncols);
  } else {
    if (C->nrows != A->nrows || C->ncols != B->ncols) {
      m4ri_die("mzd_mul_naive: Provided return matrix has wrong dimensions.\n");
    }
  }
  if(B->ncols < m4ri_radix-10) { /* this cutoff is rather arbitrary */
    mzd_t *BT = mzd_transpose(NULL, B);
    _mzd_sr_mul_naive(C, A, BT, 1);
    mzd_free (BT);
  } else {
    _mzd_sr_mul_va(C, A, B, 1);
  }
  return C;
}

mzd_t *mzd_sr_addmul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B) {
  if (C->nrows != A->nrows || C->ncols != B->ncols) {
    m4ri_die("mzd_addmul_naive: Provided return matrix has wrong dimensions.\n");
  }

  if(B->ncols < m4ri_radix-10) { /* this cutoff is rather arbitrary */
    mzd_t *BT = mzd_transpose(NULL, B);
    _mzd_sr_mul_naive(C, A, BT, 0);
    mzd_free (BT);
  } else {
    _mzd_sr_mul_va(C, A, B, 0);
  }
  return C;
}

mzd_t *_mzd_sr_mul_naive(mzd_t *C, mzd_t const *A, mzd_t const *B, const int clear) {
  wi_t eol;
  word *a, *b, *c;

  if (clear) {
    word const mask_end = C->high_bitmask;
    /* improves performance on x86_64 but is not cross plattform */
    /* asm __volatile__ (".p2align 4\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop"); */
    for (rci_t i = 0; i < C->nrows; ++i) {
      wi_t j = 0;
      for (; j < C->width - 1; ++j) {
  	C->rows[i][j] = 0;
      }
      C->rows[i][j] &= ~mask_end;
    }
  }

  if(C->ncols % m4ri_radix) {
    eol = (C->width - 1);
  } else {
    eol = (C->width);
  }

  word sum;
  wi_t const wide = A->width;
  int const blocksize = __M4RI_MUL_BLOCKSIZE;
  for (rci_t start = 0; start + blocksize <= C->nrows; start += blocksize) {
    for (rci_t i = start; i < start + blocksize; ++i) {
      a = A->rows[i];
      c = C->rows[i];
      for (rci_t j = 0; j < m4ri_radix * eol; j += m4ri_radix) {
       sum = 0;
	     for (int k = 0; k < m4ri_radix; ++k) {
          b = B->rows[j + k];
          for (wi_t ii = wide - 1; ii >= 0; --ii)
            if (a[ii] & b[ii]) { sum |= ((word)1<<k); break; }
        }
        c[j / m4ri_radix] |= sum;
      }
      
      if (eol != C->width) {
	word const mask_end = C->high_bitmask;
        /* improves performance on x86_64 but is not cross plattform */
	/* asm __volatile__ (".p2align 4\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop"); */
        sum = 0;
        for (int k = 0; k < (C->ncols % m4ri_radix); ++k) {
          b = B->rows[m4ri_radix * eol + k];
          for (wi_t ii = 0; ii < A->width; ++ii)
            if (a[ii] & b[ii]) { sum |= ((word)1<<k); break; }
        }
        c[eol] |= sum & mask_end;
      }
    }
  }

  for (rci_t i = C->nrows - (C->nrows % blocksize); i < C->nrows; ++i) {
    a = A->rows[i];
    c = C->rows[i];
    for (rci_t j = 0; j < m4ri_radix * eol; j += m4ri_radix) {
      sum = 0;
      for (int k = 0; k < m4ri_radix; ++k) {
        b = B->rows[j+k];
        for (wi_t ii = wide - 1; ii >= 0; --ii)
          if (a[ii] & b[ii]) { sum |= ((word)1<<k); break; }
      }
      c[j/m4ri_radix] |= sum;
    }
    
    if (eol != C->width) {
      word const mask_end = C->high_bitmask;
      /* improves performance on x86_64 but is not cross plattform */
      /* asm __volatile__ (".p2align 4\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop\n\tnop"); */
      sum = 0;
      for (int k = 0; k < (C->ncols % m4ri_radix); ++k) {
        b = B->rows[m4ri_radix * eol + k];
        for (wi_t ii = 0; ii < A->width; ++ii)
          if (a[ii] & b[ii]) { sum |= ((word)1<<k); break; }
      }
      c[eol] |= sum & mask_end;
    }
  }

  __M4RI_DD_MZD(C);
  return C;
}

mzd_t *_mzd_sr_mul_va(mzd_t *C, mzd_t const *v, mzd_t const *A, int const clear) {
  if(clear)
    mzd_set_ui(C, 0);

  rci_t const m = v->nrows;
  rci_t const n = v->ncols;
  
  for(rci_t i = 0; i < m; ++i)
    for(rci_t j = 0; j < n; ++j)
      if (mzd_read_bit(v,i,j))
        mzd_sr_or(C,i,0, C,i,0, A,j,0);

  __M4RI_DD_MZD(C);
  return C;
}
